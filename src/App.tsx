import React, { useState, useEffect } from 'react';
import { getChildren, OneDriveItem } from './lib/onedrive';
import { HashRouter, Link, Switch, Route } from 'react-router-dom';

const App = () =>
  <HashRouter>
    <div>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
      </ul>
      <hr />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
      </Switch>
    </div>
  </HashRouter>

export default App;

function Home() {
  const [folders, setFolders] = useState<OneDriveItem[]>([]);
  const [error, setError] = useState(undefined);

  useEffect(() => {
    getChildren()
      .then(setFolders)
      .catch(e => setError(e.toString()));
  }, []);

  return (
    <div style={{ padding: '50px' }}>
      <p>One Drive application</p>
      {error &&
        <div style={{ color: 'red' }}>Error occurred: {error}</div>
      }
      <div>
        {folders && folders.map((folder, idx) => {
          return <div key={idx}>
            {folder.name}
          </div>
        })}
      </div>
    </div>
  );
}

const About = () =>
  <div>
    <h2>About</h2>
  </div>
