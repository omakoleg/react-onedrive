import { config } from '../lib/config';

/*eslint no-restricted-globals: ["error", "event"]*/
export const forceAuth = () => {
    const parts = location.hash.split("&");
    if (parts.length > 0) {
        let partsMap = parts.map(i => i.split("="));
        let accessToken = partsMap.find(i => i[0] === '#access_token');
        if (accessToken) {
            localStorage.setItem(config.localStorageTokenName, accessToken[1]);
            window.location.href = config.siteUrl;
        }
    }
    const token = localStorage.getItem(config.localStorageTokenName)
    if (!token) {
        window.location.href = authUrl;
    }
}

export const mergeAuthHeader = (headersObject: any) => {
    headersObject["Authorization"] = "Bearer " + localStorage.getItem(config.localStorageTokenName);
    return headersObject;
}

export const logout = () => {
    localStorage.removeItem(config.localStorageTokenName);
    window.location.href = authUrl;
}

export const authUrl =
    [
        "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=" + config.oneDrive.appId,
        "scope=" + config.oneDrive.scope,
        "response_type=token",
        "redirect_uri=" + config.siteUrl
    ].join('&');
