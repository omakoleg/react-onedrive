export const config: Config = {
    localStorageTokenName: 'access-token',
    siteUrl: process.env.PUBLIC_URL || "http://localhost:3000",
    oneDrive: {
        appId: "f9be86e7-12df-4c85-971b-d864a4b56a8b",
        scope: 'Files.Read',
        baseUrl: "https://graph.microsoft.com/v1.0"
    }
};

export interface Config {
    localStorageTokenName: string;
    siteUrl: string;
    oneDrive: {
        appId: string;
        scope: string;
        baseUrl: string;
    }
}
