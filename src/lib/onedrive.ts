import { config } from "./config";
import { mergeAuthHeader, logout } from "./auth";

export interface OneDriveItem {
    [k: string]: any;
    name: string;
}

async function doRequest(fullUrl: string, options: any) {
    options.headers = mergeAuthHeader(options.headers || {});
    const response = await fetch(fullUrl, options)
    if (response.status === 401) {
        logout();
        throw Error("Unauthorized error");
    }
    return response.json();
}

export async function getChildren(): Promise<OneDriveItem[]> {
    const path = "/me/drive/root/children";
    const response = await doRequest(config.oneDrive.baseUrl + path, {
        method: "get"
    })
    return response.value;
}